Copyright (c) 2018 Fenting Lin

The ZUMA Game is an online game. 
The protagonist of the game is a stone frog, which will spit out beads of various colors. 
Surrounding the stone frog is a track carrying beads, and beads of various colors slide along the track. 
Stone frogs must stop the beads from rolling into the cave at the end of the track. 
The beads spit from the stone frog are combined with the beads on the track, and the color is the same to disappear.
The game wins when all the beads on the track are eliminated.